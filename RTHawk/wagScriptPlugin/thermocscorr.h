/***************************************************************************
 
 Copyright 2014 HeartVista Inc.  All rights reserved.
 Contact: HeartVista, Inc. <rthawk-info@heartvista.com>
 
 This file is part of the RTHawk system.
 
 $HEARTVISTA_BEGIN_LICENSE$
 
 THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF HEARTVISTA
 The copyright notice above does not evidence any	
 actual or intended publication of such source code.
 
 $HEARTVISTA_END_LICENSE$
 
 ***************************************************************************/
#ifndef THERMOCSCORR_H
#define THERMOCSCORR_H

#define USEKISSFFT

#include "RthReconFilter.h"

#ifdef USEKISSFFT
#include "kiss_fft.h"
#endif

#include <QtScript>

#include "blas.h"

#define PI (3.141592653589793)

class thermocscorr : public RthReconFilter {
  Q_OBJECT
  Q_CLASSINFO("maxInputs","3")
  Q_CLASSINFO("minInputs","3")
  Q_CLASSINFO("maxOutputs","1")
  Q_CLASSINFO("minOutputs","1")
  Q_PROPERTY(float stepsize READ stepsize WRITE setStepsize)
  Q_PROPERTY(float maskthresh READ maskthresh WRITE setMaskthresh)
  Q_PROPERTY(float stopthresh READ stopthresh WRITE setStopthresh)
  Q_PROPERTY(float tro READ tro WRITE settro)
  Q_PROPERTY(float te READ te WRITE sette)
  Q_PROPERTY(int maxcorrvoxels READ maxcorrvoxels WRITE setMaxcorrvoxels)
  Q_PROPERTY(bool driftcomp READ driftcomp WRITE setDriftcomp)
  Q_PROPERTY(bool verbose READ verbose WRITE setVerbose)
  RTH_UNIT_TEST(thermocscorr)
  RTHLOGGER_DECLARE
public:
  thermocscorr(QObject* parent = 0);
  ~thermocscorr();
public slots:
  float stepsize();
  void setStepsize(float stepsize);
  float maskthresh();
  void setMaskthresh(float maskthresh);
  float stopthresh();
  void setStopthresh(float stopthresh);
  float tro();
  void settro(float tro);
  float te();
  void sette(float te);
  int maxcorrvoxels();
  void setMaxcorrvoxels(int maxcorrvoxels);
  float improdthresh();
  void setImprodthresh(float improdthresh);
  bool driftcomp();
  void setDriftcomp(bool driftcomp);
  bool passthrough();
  void setPassthrough(bool passthrough);
  bool verbose();
  void setVerbose(bool verbose);
protected:
  void execute(QList<const RthReconData*> input, QList<RthReconData*> output);
  void cleanup();
private:
private:
  float m_stepsize;
  float m_maskthresh;
  float m_stopthresh;
  float m_tro;
  float m_te;
  bool m_driftcomp;
  int m_nro;
  int m_npe;
  int m_maxcorrvoxels;
  float m_improdthresh;
  bool m_rebuildvectors;
  bool m_passthrough;
  bool m_verbose;
  QVector<float> m_tte;
  QVector<float> m_mask;
  QVector<float> m_xm;
  QVector<float> m_baseimgr;
  QVector<float> m_baseimgi;
  QVector<float> m_datar;
  QVector<float> m_datai;
#ifdef USEKISSFFT
  kiss_fft_cfg m_fftcfg;
  QVector<kiss_fft_cpx> m_ftbuffer;
  QVector<kiss_fft_cpx> m_ftbufferout;
#else
  QVector<float> m_ftbuffer;
#endif
  QVector<float> m_mrm;
  QVector<float> m_mim;
  QVector<double> m_auxr;
  QVector<double> m_auxi;
  QVector<double> m_Ar;
  QVector<double> m_Ai;
  QVector<double> m_gr;
  QVector<double> m_gi;
};

/* l2 norm of a complex vector */
float norm2c(int n,double* inr,double* ini);

/* build system matrix for CS compensation */
void buildA(double *Ar, double *Ai, float *x, float *tte,
	    float *mr, float *mi, float *baseimgr, float *baseimgi,
	    double *auxr, double *auxi, int nro, int nx);

#ifndef USEKISSFFT

/* Num Rec C FFT */
void four1(float data[], int nn, int isign);

/* Call four1 FFT with fftshift and separate real/imag input/output */
void ft(float *ftbuffer, float *datar, float *datai, long n);

#endif

RTHRECON_DECLARE_QMETAOBJECT(thermocscorr, QObject*)
#endif
