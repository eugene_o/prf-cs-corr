/***************************************************************************
 
 Copyright 2013 HeartVista Inc.  All rights reserved.
 Contact: HeartVista, Inc. <rthawk-info@heartvista.com>
 
 This file is part of the RTHawk system.
 
 $HEARTVISTA_BEGIN_LICENSE$
 
 THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF HEARTVISTA
 The copyright notice above does not evidence any	
 actual or intended publication of such source code.
 
 $HEARTVISTA_END_LICENSE$
 
 ***************************************************************************/
#include "wagScriptPlugin.h"

#include "thermocscorr.h"
#include "thermohybrid.h"
#include "ellipsemask.h"


QStringList RthReconCustomScriptPlugin::keys() const
{
	QStringList list;
	list << QLatin1String("wag");
	list << QLatin1String("wag.scriptPlugin");
	return list;
}

void RthReconCustomScriptPlugin::initialize( const QString & key, QScriptEngine * engine )
{
	if (key == QLatin1String("wag")) {
	} else if (key == QLatin1String("wag.scriptPlugin")) {

	QScriptValue customFilterClass = engine->scriptValueFromQMetaObject<thermocscorr>();
	engine->globalObject().setProperty("thermocscorr", customFilterClass);

	QScriptValue customFilterClass2 = engine->scriptValueFromQMetaObject<thermohybrid>();
	engine->globalObject().setProperty("thermohybrid", customFilterClass2);

	QScriptValue customFilterClass3 = engine->scriptValueFromQMetaObject<ellipsemask>();
	engine->globalObject().setProperty("ellipsemask", customFilterClass3);
		
	} else {
		Q_ASSERT_X(false, "wagScriptPlugin::initialize", qPrintable(key));
	}
}

#if (QT_VERSION < 0x050000)
Q_EXPORT_PLUGIN(wagScriptPlugin)
#endif
