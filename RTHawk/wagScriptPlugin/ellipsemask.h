/***************************************************************************
 
 Copyright 2014 HeartVista Inc.  All rights reserved.
 Contact: HeartVista, Inc. <rthawk-info@heartvista.com>
 
 This file is part of the RTHawk system.
 
 $HEARTVISTA_BEGIN_LICENSE$
 
 THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF HEARTVISTA
 The copyright notice above does not evidence any	
 actual or intended publication of such source code.
 
 $HEARTVISTA_END_LICENSE$
 
 ***************************************************************************/
#ifndef ELLIPSEMASK_H
#define ELLIPSEMASK_H

#include "RthReconFilter.h"

#include <QtScript>

#define PI (3.141592653589793)
#define TWOPI (6.28318530718)

class ellipsemask : public RthReconFilter {
  Q_OBJECT
  Q_CLASSINFO("maxInputs","1")
  Q_CLASSINFO("minInputs","1")
  Q_CLASSINFO("maxOutputs","1")
  Q_CLASSINFO("minOutputs","1")
  Q_PROPERTY(float rotangle READ rotangle WRITE setRotangle)
  Q_PROPERTY(float xoffset READ xoffset WRITE setXoffset)
  Q_PROPERTY(float yoffset READ yoffset WRITE setYoffset)
  Q_PROPERTY(float xdiameter READ xdiameter WRITE setXdiameter)
  Q_PROPERTY(float ydiameter READ ydiameter WRITE setYdiameter)
  Q_PROPERTY(bool verbose READ verbose WRITE setVerbose)
  RTH_UNIT_TEST(ellipsemask)
  RTHLOGGER_DECLARE
public:
  ellipsemask(QObject* parent = 0);
  ~ellipsemask();
public slots:
  float rotangle();
  void setRotangle(float rotangle);
  float xoffset();
  void setXoffset(float xoffset);
  float yoffset();
  void setYoffset(float yoffset);
  float xdiameter();
  void setXdiameter(float xdiameter);
  float ydiameter();
  void setYdiameter(float ydiameter);
  bool verbose();
  void setVerbose(bool verbose);
protected:
  void execute(QList<const RthReconData*> input, QList<RthReconData*> output);
  void cleanup();
private:
private:
  int m_nx;
  int m_ny;
  float m_rotangle;
  float m_xoffset;
  float m_yoffset;
  float m_xdiameter;
  float m_ydiameter;
  bool m_rebuildmask;
  bool m_verbose;
  QVector<complexFloat> m_mask;
};

RTHRECON_DECLARE_QMETAOBJECT(ellipsemask, QObject*)
#endif
